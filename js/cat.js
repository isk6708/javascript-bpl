breedList2();

$('#breed').change(function(){
    let breedurl = "https://api.thecatapi.com/v1/images/search?q="+this.value;
    $.get(breedurl, function(data, status){
        // console.log(data);
        $('#imgcat').attr('src',data[0].url);
    });
})

function breedList2(){
    let breedurl = "https://api.thecatapi.com/v1/breeds";
    $.get(breedurl, function(data, status){
        const cats = data;
        let defaultcat = '';
        for(const i in cats){
            if (defaultcat == '')
                defaultcat = cats[i].image.url;

            $('#breed').append(`<option value="${cats[i].id}">
                ${cats[i].name}
            </option>`);
        }
        $('#imgcat').attr('src',defaultcat);
        
    });
}

function breedList(){
    let breedurl = "https://api.thecatapi.com/v1/breeds";
    const req = new XMLHttpRequest();
    req.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            const cats = JSON.parse(this.responseText);
            for(const i in cats){
                $('#breed').append('<option value="'+cats[i].name+'">'+cats[i].name+'</option>');
            }
        }
    }
    req.open('GET',breedurl);
    req.send();
}

